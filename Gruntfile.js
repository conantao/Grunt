module.exports = function(grunt){
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            bar: {
              src: ['src/bb.js', 'src/bbb.js'],
              dest: 'dist/b.js',
            },
        },
        uglify: {
              options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
              },
              build: {
                src: 'src/<%= pkg.name %>.js',
                dest: 'dist/<%= pkg.name %>.min.js'
              },
              // taskName:{
              //  files:{
              //   'dist/foo.min.js':['src/foo.js']
              //   }
              // }
        },
        jshint: {
          files: ['Gruntfile.js', 'src/*.js'],
          options: {
            // options here to override JSHint defaults
            globals: {
              jQuery: true,
              console: true,
              module: true,
              document: true
            }
          }
        },
        // Metadata.
        meta: {
            basePath: './',
            srcPath: 'public/sass/',
            deployPath: 'public/css/'
        },
        // Task configuration.
        sass: {
            development: {
                options: {
                    // 　　* nested：嵌套缩进的css代码，它是默认值。
                    // 　　* expanded：没有缩进的、扩展的css代码。
                    // 　　* compact：简洁格式的css代码。
                    // 　　* compressed：压缩后的css代码。
                  style : ['compressed']
                },
                files: {
                    '<%= meta.deployPath %>style.css': '<%= meta.srcPath %>style.scss'
                },
            }
        },
        less: {
          development: {
            options: {
              paths: ['public/css'],
              compress:["true"]
            },
            files: {
              'public/css/result.css': 'public/less/style.less'
            }
          },
          production: {
            options: {
              paths: ['public/css'],
              compress:["true"],
              plugins: [
                // new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
                // new (require('less-plugin-clean-css'))(cleanCssOptions)
              ],
              modifyVars: {
                imgPath: '"http://mycdn.com/path/to/images"',
                bgColor: 'red'
              }
            },
            files: {
               'public/css/dist/result.css': 'public/less/style.less'
            }
          }
        },
        watch: {
          files: ['<%= jshint.files %>','<%= meta.srcPath %>/**/*.scss'],
          tasks: ['concat','uglify',"sass"]
        }
    });

  // 加载包含 "uglify" 任务的插件。
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');





  // Default task.
  grunt.registerTask('test',['concat']);
  grunt.registerTask('default', ['uglify']);

  // A very basic default task.
  grunt.registerTask('default', 'Log some stuff.', function() {
    grunt.log.write('Logging some stuff...').ok();
  });

};



    //Contrib-jshint——javascript语法错误检查；
    //Contrib-watch——实时监控文件变化、调用相应的任务重新执行；
    //Contrib-clean——清空文件、文件夹；
    //Contrib-uglify——压缩javascript代码
    //Contrib-copy——复制文件、文件夹
    //Contrib-concat——合并多个文件的代码到一个文件中
    //karma——前端自动化测试工具
